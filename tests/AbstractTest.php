<?php

/**
 * This file is part of the Berny\Xyringe test suite
 *
 * (c) Berny Cantos <be@rny.cc>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Xyringe\Test;

abstract class AbstractTest extends \PHPUnit_Framework_TestCase
{
}
